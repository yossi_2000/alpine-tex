FROM alpine:latest

ENV PATH $PATH:/usr/local/texlive/2021/bin/x86_64-linuxmusl

RUN apk update &&\
	apk upgrade && \
	apk add --no-cache wget \
	tar \
	xz \
	poppler-utils \
	perl &&\
	wget http://ftp.jaist.ac.jp/pub/CTAN/systems/texlive/tlnet/install-tl-unx.tar.gz &&\
	tar -xvf install-tl-unx.tar.gz && \
	cd install-tl*  && \
	printf "%s\n" \
	"selected_scheme scheme-basic" \
	"option_doc 0" \
	"option_src 0" \
	> ./texlive.profile && \
	./install-tl --profile=./texlive.profile -no-gui &&\
	tlmgr update --self --all &&\
	tlmgr install collection-latexrecommended \
	collection-latexextra  \
	collection-fontsrecommended \
	collection-langjapanese  \
	collection-mathscience  \
	latexmk \
	jsclasses \
	newtx \
	simplekv \
	chktex \
	biblatex \
	ulem &&\
	kpsewhich -all texmf.cnf | xargs -I {} sed 's/openout_any = p/openout_any = a/g' -i {} &&\
	apk del --purge wget tar xz

RUN mkdir /texsrc

VOLUME /texsrc

WORKDIR /texsrc

CMD ["/bin/sh"]
